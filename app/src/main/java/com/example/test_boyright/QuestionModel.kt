package com.example.test_boyright

import kotlinx.android.extensions.ContainerOptions

data class QuestionModel(
    var id:Int,
    var question:Int,
    var options: ArrayList<optionVal>,
    var correctAnswer:Int
    )
