package com.example.test_boyright

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.test_boyright.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private  var binding : ActivityMainBinding?=null
    lateinit var answerList: ArrayList<optionVal>
    lateinit var placesAdapter : questionAdapter

    private lateinit var questions: ArrayList<QuestionModel>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding?.root)

        questions=Constants.getQuestions()
        setupRecyclerView(questions[0].options)

        binding?.btnClick?.setOnClickListener {
            binding?.btnClick?.setImageResource(R.drawable.ic_check_button)


            answerList = placesAdapter.get()



            for ( i in 0 until answerList.size ) {
                if (answerList[i].clicked && answerList[i].correct == 1) {
                    Log.i("yes","yes")
                    answerList[i].back = R.drawable.ic_correct_1
                }
                if (answerList[i].clicked && answerList[i].correct != 1) {
                    Log.i("yes","yes")
                    answerList[i].back = R.drawable.ic_wrong_1
                }

            }
            placesAdapter = questionAdapter(this, answerList)
            binding?.rvItemMain?.adapter = placesAdapter
        }

    }

    private fun setupRecyclerView(
        choiceList: ArrayList<optionVal>
    ){

        binding?.rvItemMain?.setHasFixedSize(true)

         placesAdapter = questionAdapter(this, choiceList)
        binding?.rvItemMain?.adapter = placesAdapter}



    }