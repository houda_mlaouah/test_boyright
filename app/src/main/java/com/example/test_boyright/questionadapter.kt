package com.example.test_boyright

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.test_boyright.databinding.ItemRecyclerBinding
import kotlinx.android.synthetic.main.item_recycler.view.*


open class questionAdapter(
    private val context: Context,
    private var list: ArrayList<optionVal>,
    ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var listall: ArrayList<QuestionModel>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        listall =Constants.getQuestions()
        return MyViewHolder(
            ItemRecyclerBinding.inflate(LayoutInflater.from(context),
                parent,
                false
            )
        )
    }
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.imageView3.setImageResource(list[position].back)
        if (holder is MyViewHolder) {
            Glide.with(context)
                .load(model.image)
                .into(holder.binding.imageView2)

            holder.binding?.itemView.setOnClickListener {
                model.clicked = true
                holder.binding?.imageView3.setImageResource(R.drawable.ic_click_stat)
            }
        }
    }
    override fun getItemCount(): Int {
        return list.size
    }
    private class MyViewHolder(binding: ItemRecyclerBinding) : RecyclerView.ViewHolder(binding.root){
        val binding = binding
    }
    fun get(): ArrayList<optionVal> {
        return list
    }


}
