package com.example.test_boyright

object Constants {
    const val USER_NAME: String= "user_name"
    const val TOTAL_QUESTIONS: String= "total_question"
    const val CORRECT_ANSWERS: String= "correct_answers"

    fun getQuestions(): ArrayList<QuestionModel>{
        val  questionsList = ArrayList<QuestionModel>()
        val que1 = QuestionModel(1,
            R.drawable.ic_question_1,
           arrayListOf(
               optionVal(
                   1,
                   R.drawable.ic_boy_right,
                   false,
                   1,
                   R.drawable.ic_start
           ),optionVal(
                   2,
                   R.drawable.ic_boy_left2,
                   false,
                   2,
                   R.drawable.ic_start

               ),optionVal(
                   3,
                   R.drawable.ic_boy_left,
                   false,
                   1,

                   R.drawable.ic_start


               ),optionVal(
                   4,
                   R.drawable.ic_fille_right,
                   false,
                   2,

                   R.drawable.ic_start


               )
           ), 1

        )

        questionsList.add((que1))
        return questionsList

    }
}